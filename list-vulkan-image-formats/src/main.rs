use std::sync::Arc;

use vulkano::{
    device::physical::PhysicalDevice,
    format::Format,
    instance::{Instance, InstanceCreateInfo},
    VulkanLibrary,
};

fn main() {
    let library = VulkanLibrary::new().unwrap();
    let instance =
        Instance::new(library, InstanceCreateInfo::application_from_cargo_toml()).unwrap();

    for physical_device in instance.enumerate_physical_devices().unwrap() {
        let properties = physical_device.properties();
        println!("{}", properties.device_name);
        list_image_formats(&physical_device);
    }
}

fn list_image_formats(physical_device: &Arc<PhysicalDevice>) {
    let all_formats = all_formats();
    for format in all_formats {
        let format_properties = physical_device.format_properties(format).unwrap();
        println!("{:?}: {:#?}", format, format_properties);
    }
}

fn all_formats() -> Vec<Format> {
    let mut result = Vec::new();
    result.push(Format::R32G32B32A32_SFLOAT);
    result
}
