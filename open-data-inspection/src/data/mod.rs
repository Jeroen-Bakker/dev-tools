use chrono::{DateTime, Utc};
use serde::Deserialize;

mod distribution;

pub use distribution::*;

pub type Records = Vec<Record>;

#[derive(Clone, Debug, Deserialize)]
pub struct Record {
    pub created_at: DateTime<Utc>,
    pub data: Vec<Entry>,
}

#[derive(Clone, Debug, Deserialize)]
pub struct Entry {
    pub benchmark_launcher: BenchmarkLauncher,
    pub benchmark_script: BenchmarkScript,
    pub blender_version: BlenderVersion,
    pub device_info: DeviceInfo,
    pub system_info: SystemInfo,
    pub scene: Scene,
    pub stats: Stats,
    pub timestamp: String,
}

#[derive(Clone, Debug, Deserialize)]
pub struct BenchmarkLauncher {
    pub checksum: String,
    pub label: String,
}
#[derive(Clone, Debug, Deserialize)]
pub struct BenchmarkScript {
    pub checksum: String,
    pub label: String,
}

#[derive(Clone, Debug, Deserialize)]
pub struct BlenderVersion {
    pub build_commit_date: String,
    pub build_commit_time: String,
    pub build_date: String,
    pub build_hash: String,
    pub build_time: String,
    pub checksum: String,
    pub label: String,
    pub version: String,
}
#[derive(Clone, Debug, Deserialize)]
pub struct DeviceInfo {
    pub device_type: DeviceType,
    pub compute_devices: Vec<ComputeDevice>,
    pub num_cpu_threads: u32,
}

pub type DeviceTypes = Vec<DeviceType>;

#[derive(Copy, Clone, Debug, Deserialize, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub enum DeviceType {
    CPU,
    HIP,
    OPTIX,
    CUDA,
    METAL,
    ONEAPI,
    OPENCL,
}

#[derive(Clone, Debug, Deserialize)]
pub struct ComputeDevice {
    pub name: String,
    pub r#type: DeviceType,
}

#[derive(Clone, Debug, Deserialize)]
pub struct Scene {
    pub label: String,
    pub checksum: String,
}

#[derive(Clone, Debug, Deserialize)]
pub struct Stats {
    #[serde(default)]
    pub time_limit: f64,
    #[serde(default)]
    pub time_for_samples: f64,
    #[serde(default)]
    pub number_of_samples: u64,
    pub total_render_time: f64,
    pub device_peak_memory: f64,
    #[serde(default)]
    pub samples_per_minute: f64,
    pub render_time_no_sync: f64,
}

#[derive(Clone, Debug, Deserialize)]
pub struct SystemInfo {
    pub system: SystemType,
    pub bitness: String,
    pub machine: String,
    pub dist_name: String,
    pub dist_version: String,
    pub num_cpu_cores: u64,
    pub num_cpu_sockets: u64,
    pub num_cpu_threads: u64,
    pub devices: Vec<ComputeDevice>,
}

#[derive(Clone, Debug, Deserialize, PartialEq)]
pub enum SystemType {
    Windows,
    Linux,
    Darwin,
}
