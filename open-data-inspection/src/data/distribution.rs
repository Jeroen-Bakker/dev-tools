use super::SystemInfo;

pub type Distributions = Vec<Distribution>;

#[derive(Debug, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub struct Distribution {
    pub name: String,
    pub version: String,
}

impl From<&SystemInfo> for Distribution {
    fn from(value: &SystemInfo) -> Self {
        Self {
            name: value.dist_name.clone(),
            version: value.dist_version.clone(),
        }
    }
}
