use crate::{data::Records, queries::Queries};

pub trait DistributionDeviceTypeUsage {
    fn print_report_distribution_device_type_usage(&self);
}

impl DistributionDeviceTypeUsage for Records {
    fn print_report_distribution_device_type_usage(&self) {
        let distributions = self.query_linux_distributions();
        let device_types = self.query_device_types();

        let max_distribution_name_len = distributions.iter().map(|d| d.name.len()).max().unwrap();
        let max_distribution_version_len =
            distributions.iter().map(|d| d.version.len()).max().unwrap();

        // Header
        print!(
            "| {:<width$} ",
            "Distribution",
            width = max_distribution_name_len
        );
        print!(
            "| {:<width$} ",
            "Version",
            width = max_distribution_version_len
        );
        for device_type in &device_types {
            print!("| {:^6} ", format!("{:?}", device_type))
        }
        println!("|");

        // Separator
        print!("| {:-<width$} ", "", width = max_distribution_name_len);
        print!("| {:-<width$} ", "", width = max_distribution_version_len);
        for _device_type in &device_types {
            print!("| ------ ")
        }
        println!("|");

        let mut last_distribution_name = String::default();

        for distribution in &distributions {
            print!(
                "| {:width$} ",
                if distribution.name != last_distribution_name {
                    &distribution.name
                } else {
                    ""
                },
                width = max_distribution_name_len
            );
            last_distribution_name = distribution.name.clone();
            print!(
                "| {:width$} ",
                distribution.version,
                width = max_distribution_version_len
            );
            for device_type in &device_types {
                let count_entries = self
                    .query_distribution_device_type(distribution, *device_type)
                    .len();
                print!(
                    "| {:6} ",
                    if count_entries != 0 {
                        count_entries.to_string()
                    } else {
                        String::default()
                    }
                );
            }
            println!("|");
        }
    }
}
