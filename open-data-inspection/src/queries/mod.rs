use std::collections::HashSet;

use chrono::{DateTime, Utc};

use crate::data::{
    DeviceType, DeviceTypes, Distribution, Distributions, Record, Records, SystemType,
};

pub trait Aspects {
    fn is_linux(&self) -> bool;
    fn days_in_past(&self) -> i64;
}

impl Aspects for Record {
    fn is_linux(&self) -> bool {
        self.data[0].system_info.system == SystemType::Linux
    }

    fn days_in_past(&self) -> i64 {
        let utc: DateTime<Utc> = Utc::now();
        let timespan = utc - self.created_at;
        timespan.num_days()
    }
}

pub trait Queries {
    fn query_linux_distributions(&self) -> Distributions;
    fn query_last_year(&self) -> Records;
    fn query_device_types(&self) -> DeviceTypes;
    fn query_distribution_device_type(
        &self,
        distribution: &Distribution,
        device_type: DeviceType,
    ) -> Records;
    fn query_distribution(&self, distribution: &Distribution) -> Records;

    fn query_min_distribution_sample_size(&self, min_sample_size: usize) -> Records;
}

impl Queries for Records {
    fn query_last_year(&self) -> Records {
        self.iter()
            .filter(|r| r.days_in_past() < 366)
            .cloned()
            .collect::<Records>()
    }

    fn query_linux_distributions(&self) -> Distributions {
        let result = self
            .iter()
            .filter(|r| r.is_linux())
            .map(|r| Distribution::from(&r.data[0].system_info))
            .collect::<HashSet<Distribution>>();

        let mut result = result.into_iter().collect::<Distributions>();
        result.sort();
        result
    }

    fn query_device_types(&self) -> DeviceTypes {
        let result = self
            .iter()
            .map(|r| r.data[0].device_info.device_type)
            .collect::<HashSet<DeviceType>>();

        let mut result = result.into_iter().collect::<DeviceTypes>();
        result.sort();
        result
    }

    fn query_distribution_device_type(
        &self,
        distribution: &Distribution,
        device_type: DeviceType,
    ) -> Records {
        self.into_iter()
            .filter(|r| &Distribution::from(&r.data[0].system_info) == distribution)
            .filter(|r| r.data[0].device_info.device_type == device_type)
            .cloned()
            .collect::<Records>()
    }
    fn query_distribution(&self, distribution: &Distribution) -> Records {
        self.into_iter()
            .filter(|r| &Distribution::from(&r.data[0].system_info) == distribution)
            .cloned()
            .collect::<Records>()
    }

    fn query_min_distribution_sample_size(&self, min_sample_size: usize) -> Records {
        let distributions = self
            .query_linux_distributions()
            .into_iter()
            .filter(|d| self.query_distribution(d).len() > min_sample_size)
            .collect::<Distributions>();
        self.into_iter()
            .filter(|r| distributions.contains(&Distribution::from(&r.data[0].system_info)))
            .cloned()
            .collect::<Records>()
    }
}
