use queries::Queries;
use serde_jsonlines::json_lines;

use crate::reports::distribution_device_type_usage::DistributionDeviceTypeUsage;

mod data;
mod queries;
mod reports;

fn main() -> std::io::Result<()> {
    let records = load_opendata_entries()?;
    let records = records.query_last_year();
    let records = records.query_min_distribution_sample_size(16);
    records.print_report_distribution_device_type_usage();
    Ok(())
}

fn load_opendata_entries() -> std::io::Result<data::Records> {
    let iter = json_lines::<data::Record, &str>("input-data/opendata.jsonl")?;
    let mut records = Vec::new();
    for r in iter {
        if let Ok(record) = r {
            records.push(record);
        }
    }
    Ok(records)
}
