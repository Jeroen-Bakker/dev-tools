# Distribution Device Type Report

Report was generated on 2023-09-19.

This report shows the number of open data enties with the next rules. 

* Only entries that were run on a Linux OS.
* Only entries not older than 1 year.
* Only distributions with at least 16 entries.

Blender doesn't track any user data and therefore we rely on data that have been send
to us willingly. This data would have been more useful when we also could include the
linux kernel version. Or the distribution version would be more fine grained. 


| Distribution             | Version |  CPU   |  HIP   | OPTIX  |  CUDA  | ONEAPI |
| ------------------------ | ------- | ------ | ------ | ------ | ------ | ------ |
| AlmaLinux                | 9.1     | 17     |        |        |        |        |
| Arch Linux               |         | 147    | 37     | 97     |        | 7      |
|                          | rolling | 454    | 108    | 206    | 1      | 2      |
| ArcoLinux                | rolling | 23     | 1      | 5      |        |        |
| Artix Linux              |         | 9      |        | 10     |        |        |
| Debian GNU/Linux         | 10      | 14     |        | 15     | 2      |        |
|                          | 11      | 71     |        | 15     | 29     |        |
|                          | 12      | 79     | 4      | 27     | 40     |        |
|                          | n/a     | 104    | 3      | 14     | 30     |        |
|                          | testing | 22     |        | 2      | 1      |        |
| Devuan GNU/Linux         | 5       | 13     | 4      | 4      |        |        |
| EndeavourOS              | rolling | 141    | 26     | 57     | 2      | 1      |
| Fedora Linux             | 36      | 109    |        | 42     |        |        |
|                          | 37      | 332    | 20     | 68     |        | 4      |
|                          | 38      | 293    | 23     | 101    | 2      |        |
| Garuda Linux             | Soaring | 45     | 9      | 16     |        |        |
| Gentoo                   | 2.13    | 16     | 4      | 9      |        | 2      |
|                          | 2.9     | 23     | 2      | 3      |        |        |
| KDE neon                 | 22.04   | 20     | 1      | 7      |        |        |
| Linux Mint               | 20.3    | 33     |        | 28     | 2      |        |
|                          | 21      | 81     | 1      | 38     | 2      |        |
|                          | 21.1    | 115    | 24     | 82     | 2      |        |
|                          | 21.2    | 27     | 2      | 18     | 1      |        |
| Manjaro Linux            | 22.0    | 13     | 1      | 10     |        |        |
|                          | 22.0.0  | 97     | 10     | 49     | 1      |        |
|                          | 22.0.1  | 8      |        | 10     |        |        |
|                          | 22.0.4  | 21     |        | 10     | 1      |        |
|                          | 22.0.5  | 11     | 2      | 5      |        |        |
|                          | 22.1.0  | 21     | 2      | 15     |        |        |
|                          | 23.0.0  | 82     | 14     | 27     |        | 3      |
| NixOS                    | 23.05   | 20     | 1      | 1      |        |        |
| Nobara Linux             | 36      | 15     | 8      | 4      |        |        |
|                          | 37      | 13     | 1      | 10     |        |        |
| Pop!_OS                  | 20.04   | 19     |        | 3      |        |        |
|                          | 22.04   | 195    | 4      | 159    |        |        |
| Red Hat Enterprise Linux | 9.2     | 22     |        | 1      |        |        |
| Rocky Linux              | 8.7     | 24     |        | 1      |        |        |
| SteamOS                  | 3.4.4   | 18     |        |        |        |        |
| TUXEDO OS                | 22.04   | 14     |        | 14     |        |        |
| Ubuntu                   | 18.04   | 17     |        | 11     | 10     |        |
|                          | 20.04   | 154    | 33     | 113    | 12     | 3      |
|                          | 22.04   | 1115   | 38     | 722    | 27     | 9      |
|                          | 22.10   | 201    | 10     | 63     | 6      |        |
|                          | 23.04   | 178    |        | 99     |        | 1      |
| Zorin OS                 | 16      | 9      |        | 8      |        |        |
| openSUSE Leap            | 15.4    | 23     |        | 7      |        |        |